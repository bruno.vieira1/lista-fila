package Quest1;

import java.util.Scanner;
import java.util.Stack;

public class Pilha {
	static Scanner sc = new Scanner(System.in);
	static Stack<Livro> pilha = new Stack<Livro>();

	public static void main(String[] args) {

		operacoes(pilha);
	}

	public static void operacoes(Stack<Livro> pilha) {

		Integer op = 0;
		do {
			System.out.println(
					"Informe as opera��es que deseja efetuar:\n1=Inserir livro\n2=Consultar livro\n3=Remover liro\n4=Esvaziar na Pilha\n5=Sair");
			op = sc.nextInt();
			sc.nextLine();
			switch (op) {
			case 1:
				pilha.add(addLivro());
				System.out.println("Livro adicionado na pilha.");
				break;

			case 2:
				try {
					for (Livro livro : pilha) {
						System.out.println(livro);
					}
				} catch (Exception e) {
					System.out.println("N�o existe pilha de livros no momento.");
				}
				break;

			case 3:
				System.out.println("Informe o liro a ser removido: ");
				pilha.remove(0);

				break;

			case 4:
				try {
					pilha.clear();
					System.out.println("A pilha est� vazia!");
				} catch (Exception e) {
					System.out.println("Ocorreu um erro ao limpar a lista.");
				}
				break;

			case 5:
				System.out.println("programa terminado.");
				System.exit(0);
				break;

			default:
				break;
			}
		} while (op != 5);

	}

	public static Livro addLivro() {
		System.out.println("Informar nome do livro: ");
		String livro = sc.nextLine();
		return new Livro(livro);
	}

}
