package Quest2;

public class Palindromo {
	private String str;

	public Palindromo() {

	}

	public Palindromo(String str) {
		super();
		this.str = str;
	}

	public String getStr() {
		return str;
	}

	public void setStr(String str) {
		this.str = str;
	}

	@Override
	public String toString() {
		return "Palindromo [str=" + str + "]";
	}

}
