package Quest4;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Pilha {

	public static int posicaoPilha = 0;
	public static List<Object> pilha;
	private static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		do {
			System.out.println("Informe a opera��o que deseja realizar:");
			System.out.print(
					"\n1=Escreva a fun��o inicializarPilha \n2=Escreva a fun��o push para adicionar um elemento � pilha \n3=Escreva a fun��o pop para remover \n4=Sair");
			int op = sc.nextInt();
			sc.nextLine();
			switch (op) {
			case 1: {
				System.out.println("A pilha foi inicializada!");
				inicializarPilha();
				break;
			}
			case 2: {
				System.out.println("O que deseja adicionar no topo da pilha?");
				Object a = sc.nextLine();
				push(pilha, a);
				break;
			}
			case 3: {
				pop(pilha);
				break;
			}
			case 4: {
				System.out.println("Obrigado por utilizar os nossos servi�os.");
				System.exit(0);
				break;
			}
			default:
				System.out.println("Opo no cadastrada!");
			}
			if (!pilha.isEmpty()) {
				for (Object obj : pilha) {
					System.out.println(obj);
				}
			}

		} while (true);

	}

	public static void inicializarPilha() {
		pilha = new ArrayList<Object>();
	}

	public static void push(List<Object> list, Object obj) {
		list.add(obj);
	}

	public static Object pop(List<Object> list) {
		int posicao = list.size() - 1;
		Object ret = list.get(posicao);
		list.remove(posicao);
		return ret;
	}

}
