package Quest4;

public class Alunos {
	String nome;
	Integer idade;

	public Alunos(String nome, Integer idade) {
		super();
		this.nome = nome;
		this.idade = idade;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Integer getIdade() {
		return idade;
	}

	public void setIdade(Integer idade) {
		this.idade = idade;
	}

	@Override
	public String toString() {
		return "Alunos [nome=" + nome + ", idade=" + idade + "]";
	}

}
