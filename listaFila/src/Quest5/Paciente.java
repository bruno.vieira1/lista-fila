package Quest5;

public class Paciente {
	String nome;

	public Paciente() {

	}

	public Paciente(String nome) {
		super();
		this.nome = nome;
	}

	@Override
	public String toString() {
		return "Paciente [nome=" + nome + "]";
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

}
